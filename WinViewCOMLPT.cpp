#include <windows.h>
#include <setupapi.h>
#include <tchar.h>
#include <cstdlib>
#include <iostream>

#pragma comment(lib, "setupapi.lib")

int EnumerateSerialPorts()
{
    HDEVINFO hDevInfo;
    SP_DEVINFO_DATA devInfoData;
    DWORD i = 0;
    TCHAR deviceName[256] = { 0 };
    DWORD size = 0;

    hDevInfo = SetupDiGetClassDevs(NULL, TEXT("PORTS"), NULL, DIGCF_PRESENT | DIGCF_ALLCLASSES);

    if (hDevInfo == INVALID_HANDLE_VALUE)
    {
        std::cerr << "Failed to get device information set." << std::endl;
        return EXIT_FAILURE;
    }

    devInfoData.cbSize = sizeof(SP_DEVINFO_DATA);

    while (SetupDiEnumDeviceInfo(hDevInfo, i, &devInfoData))
    {
        if (SetupDiGetDeviceRegistryProperty(hDevInfo, &devInfoData, SPDRP_FRIENDLYNAME, NULL,
            (PBYTE)deviceName, sizeof(deviceName), &size))
        {
            if (_tcsstr(deviceName, TEXT("COM")) || _tcsstr(deviceName, TEXT("LPT")))
            {
                std::wcout << deviceName << std::endl;
            }
        }
        i++;
    }

    SetupDiDestroyDeviceInfoList(hDevInfo);
    return EXIT_SUCCESS;
}

int main()
{
    std::cout << "Enumerating COM and LPT ports:" << std::endl;
    int result = EnumerateSerialPorts();
    return result;
}
